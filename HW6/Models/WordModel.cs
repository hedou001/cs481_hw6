﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW6.Models
{
    class WordModel
    {
        // JSON Implementation of a complete word and its definitions
        [JsonProperty("definitions")]
        public IList<WordDefModel> Defs { get; set; }

        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }

        [JsonIgnore]
        public string ErrorMessage { get; set; }
    }
}
