﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW6.Models
{
    // JSON Implementation of one definition of a word
    class WordDefModel
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Def { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public string Emoji { get; set; }
    }
}
