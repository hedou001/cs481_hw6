﻿using System;
using System.Net.Http;
using HW6.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace HW6
{
    class OwlController
    {
        static HttpClient cl = new HttpClient();

        // Function that makes the request to Owlbot API
        public static async Task<WordModel> GetWord(string word)
        {
            var uri = new Uri("https://owlbot.info/api/v4/dictionary/" + word);
            var request = new HttpRequestMessage();
            WordModel data = new WordModel();

            data.Defs = null;
            data.Word = null;
            data.ErrorMessage = null;
            if (!WordIsValid(word))
            {
                data.ErrorMessage = "Word is invalid.";
                return data;
            }
            // Creating the request
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Authorization", "Token af516f25ba402a15a5f07e1f2156981db6ecd60b");
            try {
                // Sending the request and waiting for the response
                HttpResponseMessage resp = await cl.SendAsync(request);
                // Acting in consequence of the response status code
                switch (resp.StatusCode) {
                    case System.Net.HttpStatusCode.OK:
                        // Converting the JSON received in a C# model
                        data = JsonConvert.DeserializeObject<WordModel>(await resp.Content.ReadAsStringAsync());
                        break;
                    case System.Net.HttpStatusCode.NotFound:
                        data.ErrorMessage = "Word not found.";
                        break;
                    default:
                        data.ErrorMessage = "Invalid response received.";
                        break;
                }
            } catch {
                data.ErrorMessage = "No internet connection.";
            }
            return data;
        }

        // Checks if there is no special char in the word (works only for english)
        private static bool WordIsValid(string word)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

            return regexItem.IsMatch(word);
        }
    }
}
