﻿using HW6.Models;
using Plugin.Connectivity;
using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace HW6
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            SetAllGood();
            // Creating the callback for the Internet connection change event
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if (args.IsConnected)
                {
                    SetAllGood();
                    EnableInput();
                }
                else
                {
                    DisableInput();
                    SetErrorMessage("No internet connection.");
                }
            };
        }

        // Search button clicked
        private async void Button_Clicked(object sender, EventArgs e)
        {
            SetAllGood();
            // Error management
            if (wordEntry.Text == null || wordEntry.Text == "")
            {
                SetErrorMessage("You didn't enter a word.");
                return;
            }
            DisableInput();
            // Making the call to the API using the controller
            WordModel res = await OwlController.GetWord(wordEntry.Text);
            // Checking for errors
            if (res.ErrorMessage != null)
            {
                SetErrorMessage(res.ErrorMessage);
            }
            else
            {
                DefStack.Children.Clear();
                WordLabel.Text = res.Word;
                PronunciationLabel.Text = res.Pronunciation;
                if (res.Defs != null && res.Defs.Count != 0)
                {
                    // Dynamically filling the empty StackLayout with multiple definitions
                    TypeLabel.Text = res.Defs[0].Type;
                    for (int i = 0; i < res.Defs.Count; i++)
                    {
                        string tmp = (i + 1) + ". " + res.Defs[i].Type + ": " + res.Defs[i].Def + " e.g. " + res.Defs[i].Example;
                        StackLayout block = new StackLayout{ Orientation = StackOrientation.Horizontal };
                        StackLayout def = new StackLayout { Spacing = 0.0f };

                        block.Children.Add(new Label
                        {
                            Text = "" + (i + 1),
                            TextColor = Color.Black,
                            FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                            VerticalOptions = LayoutOptions.Start,
                            VerticalTextAlignment = TextAlignment.Center
                        });
                        def.Children.Add(new Label {
                            Text = res.Defs[i].Type,
                            FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                            VerticalOptions = LayoutOptions.Center,
                            VerticalTextAlignment = TextAlignment.Center
                        });
                        def.Children.Add(new Label
                        {
                            Text = res.Defs[i].Def + (res.Defs[i].Example != "" && res.Defs[i].Example != null ? " e.g. " + res.Defs[i].Example : ""),
                            TextColor = Color.Black,
                            FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                            VerticalOptions = LayoutOptions.Center,
                            VerticalTextAlignment = TextAlignment.Center
                        });
                        block.Children.Add(def);
                        DefStack.Children.Add(block);
                    }
                }
            }
            wordEntry.Text = "";
            EnableInput();
        }

        // Display an error message just below the search bar
        private void SetErrorMessage(string msg)
        {
            ErrorLabel.Text = msg;
            ErrorLabel.IsVisible = true;
            wordEntry.Text = "";
        }

        // Erase the error message
        private void SetAllGood()
        {
            ErrorLabel.Text = "";
            ErrorLabel.IsVisible = false;
        }

        // Disable the search button and the textbox
        private void DisableInput()
        {
            wordEntry.IsEnabled = false;
            searchBtn.IsEnabled = false;
        }

        // Enable the search button and the textbox
        private void EnableInput()
        {
            wordEntry.IsEnabled = true;
            searchBtn.IsEnabled = true;
        }
    }
}
